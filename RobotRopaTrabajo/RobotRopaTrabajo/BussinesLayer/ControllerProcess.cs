﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RobotRopaTrabajo.dataaccess;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RobotRopaTrabajo.model;
using RobotRopaTrabajo.util;

namespace RobotRopaTrabajo.BussinesLayer
{
    public class ControllerProcess
    {


        public interfaces.IProviderConnectionStaticMail ConnectionMail;
 

        private enum SPs
        {
            IndumentariaProcess, IndumentariagetReporte, IndumentariaReporteAreaSinDetalle, IndumentariaReporteSinDetalle
        }

        private ManagerCommandDB _ManagerDB = null;
        private ManagerCommandDB ManagerDB
        {
            get
            {
                if (_ManagerDB == null) _ManagerDB = new ManagerCommandDB();

                return _ManagerDB;
            }
        }

        protected Boolean IsTestMode
        {
            get
            {
                string ModoTest = ConfigurationManager.AppSettings["DEBUG_MODE"].ToString();
                if (ModoTest == "TRUE")
                    return true;
                else
                    return false;
            }
        }

        protected string GetEmail_Test()
        {
            return  ConfigurationManager.AppSettings["Test_MailDestino"].ToString();
        }

        protected string GetFolderDestination()
        {
            return ConfigurationManager.AppSettings["Carpeta_Destino"].ToString();
        }

        protected string GetEmail_Encargado_Test()
        {
            return ConfigurationManager.AppSettings["Test_MailEncargado"].ToString();
        }


        protected string GetEmail_Encargado()
        {
            return ConfigurationManager.AppSettings["Mail_Encargado"].ToString();
        }

        public void Run()
        {

           var MailList = new List<ItemForSend>();
           string Asunto = "Solicitud de ropa de trabajo";
           string Temporada = "";
           int PeriodoID = 0;

           DataTable dt= ManagerDB.RunStoreProcedure(SPs.IndumentariaProcess.ToString(), null);

           if (dt.Rows.Count > 0)
           {

               foreach (DataRow dr in dt.Rows)
               {
                   string MailTo = "";

                   if (this.IsTestMode)
                       MailTo = this.GetEmail_Test();
                   else
                       MailTo = dr["Email"].ToString();

                   Temporada = dr["temporada"].ToString();
                   PeriodoID = int.Parse(dr["periodoId"].ToString());

                   StringBuilder Body = getBodySolicitante();
                   if (MailTo != null && MailTo != "")
                    MailList.Add(new ItemForSend(MailTo, Asunto, Body.ToString()));

                   //break;
               }

              

               ValoresConfigMail configMail = new model.ValoresConfigMail();
               util.EnviarMail ManagerMail = new util.EnviarMail(configMail);


               foreach (var item in MailList)
               {
                   ManagerMail.Enviar(item, null);
               }


               //Mail para encargado
                 string AsuntoEncargado = "Reportes solicitudes de ropa de trabajo";

                 StringBuilder BodyEncargado = getBodyEncargado(Temporada,PeriodoID);

                 Exportar Exportador = new Exportar(this.GetFolderDestination());

                 string[] attachmentFilename = new string[2];
                 
                 for (int i = 0; i < 3; i++)
                 {
                     attachmentFilename[i] = Exportador.ExportaXLS(getBodyEncargadoConAttach(Temporada, PeriodoID,i), this.GetFolderDestination());    
                 }
                 

                 ItemForSend ItemEncargado;
                 if (this.IsTestMode)
                     ItemEncargado = new ItemForSend(GetEmail_Encargado_Test(), AsuntoEncargado, BodyEncargado.ToString());   
                   else
                     ItemEncargado = new ItemForSend(GetEmail_Encargado(), AsuntoEncargado, BodyEncargado.ToString());


                 ManagerMail.Enviar(ItemEncargado, attachmentFilename);


           }


        }

        private StringBuilder getBodyEncargadoConAttach(string Temporada, int PeriodoID, int tipoReporte)
        {
            StringBuilder BodyEncargado = new StringBuilder();

            BodyEncargado.Append("<Table>");

            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("&nbsp;");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");

            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("<strong>Reporte solicitudes de ropa de trabajo - " + Temporada + "</strong>" );
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");
            

            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("</Table>");

            switch (tipoReporte)
            {
                case 1:
                    BodyEncargado.Append(GetReporte(PeriodoID));
                    break;

                case 2:
                    BodyEncargado.Append(GetSegundoReporte(PeriodoID));
                    break;

                case 3:
                    BodyEncargado.Append(GetTercerReporte(PeriodoID));
                    break;
            }


            BodyEncargado.Append("<Table>");


            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");

            BodyEncargado.Append("</Table>");

            BodyEncargado.Append(GetReporte(PeriodoID));

            return BodyEncargado;
        }       

        private  StringBuilder getBodyEncargado(string Temporada,int PeriodoID)
        {
            StringBuilder BodyEncargado = new StringBuilder();

            BodyEncargado.Append("<Table>");

            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("&nbsp;");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");

            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td style=\"padding-bottom: 10px;\">");
            BodyEncargado.Append("Como conclusión de la temporada " + Temporada + ", adjunto se le remite los pedidos realizados del personal.");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");


            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("&nbsp;");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");


            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("&nbsp;");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");

            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("Muchas gracias.");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");


            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td>");
            BodyEncargado.Append("&nbsp;");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");

            BodyEncargado.Append("<tr>");
            BodyEncargado.Append("<td style=\"padding-bottom: 10px; color: #888888; \">");
            BodyEncargado.Append("Este mensaje fue enviado desde la Intranet de SCJ Conosur, no debe responder al mismo.");
            BodyEncargado.Append("</td>");
            BodyEncargado.Append("</tr>");

            BodyEncargado.Append("</Table>");

            return BodyEncargado;
        }

        private  StringBuilder getBodySolicitante()
        {
            StringBuilder Body = new StringBuilder();

            Body.Append("<Table>");

            Body.Append("<tr>");
            Body.Append("<td style=\"padding-bottom: 10px;\">");
            Body.Append("Estimado/a");
            Body.Append("</td>");
            Body.Append("</tr>");

            Body.Append("<tr>");
            Body.Append("<td style=\"padding-bottom: 10px;\">");
            Body.Append("Le comunicamos que el trámite de pedido de ropa de trabajo ha sido finalizado.");
            Body.Append("</td>");
            Body.Append("</tr>");


            Body.Append("<tr>");
            Body.Append("<td>");
            Body.Append("&nbsp;");
            Body.Append("</td>");
            Body.Append("</tr>");


            Body.Append("<tr>");
            Body.Append("<td>");
            Body.Append("Muchas gracias.");
            Body.Append("</td>");
            Body.Append("</tr>");


            Body.Append("<tr>");
            Body.Append("<td>");
            Body.Append("&nbsp;");
            Body.Append("</td>");
            Body.Append("</tr>");

            Body.Append("<tr>");
            Body.Append("<td style=\"padding-bottom: 10px; color: #888888; \">");
            Body.Append("Este mensaje fue enviado desde la Intranet de SCJ Conosur, no debe responder al mismo.");
            Body.Append("</td>");
            Body.Append("</tr>");

            Body.Append("</Table>");
            return Body;
        }


        private  string GetReporte(int PeriodoId)
        {
            SqlParameter param = new SqlParameter();
            param.DbType = DbType.Int32;
            param.ParameterName = "@PeriodoID";
            param.Value = PeriodoId;
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(param);

            DataTable dt = ManagerDB.RunStoreProcedure(SPs.IndumentariagetReporte.ToString(), parametros);

            StringBuilder Body = new StringBuilder();



         
            Body.Append("<Table>");

            Body.Append("<tr>");
            Body.Append("<td>");
            Body.Append("<strong> AREA </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> LEGAJO </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> NOMBRE Y APELLIDO </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> ZAPATOS </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> PANTALÓN </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> TORSO </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> BRIGADISTA </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> PEDIDO ESPECIAL </strong> ");
            Body.Append("</td>");
            Body.Append("</tr>");

            foreach (DataRow dr in dt.Rows)
            {

                   Body.Append("<tr style='background-color:#eeeeee'>");
                    //Area                    
                    Body.Append("<td>");
                    Body.Append(" "+ dr["Area"].ToString() + "  ");
                    Body.Append("</td>");

                   //Legajo                    
                    Body.Append("<td>");
                    Body.Append(" " + dr["legajo"].ToString() + " ");
                    Body.Append("</td>");

                    //Nombre y Apellido                    
                    Body.Append("<td>");
                    Body.Append(" " + dr["usuario"].ToString() + " ");
                    Body.Append("</td>");
              

                    //Zapatos                
                    Body.Append("<td>");
                    Body.Append(" " + dr["zapatos"].ToString() + " ");
                    Body.Append("</td>");

                   //Pantalon
                    Body.Append("<td>");
                    Body.Append(" " + dr["pantalon"].ToString() + " ");
                    Body.Append("</td>");

                    //Torso
                    Body.Append("<td>");
                    Body.Append(" " + dr["torso"].ToString() + " ");
                    Body.Append("</td>");
              

                    //Brigadista
                    Body.Append("<td>");
                    if(dr["Brigadista"].ToString().Trim().ToLower() == "true")
                     Body.Append("SI");
                    else
                     Body.Append("NO");
                    Body.Append("</td>");

                    //Pedido Especial
                    Body.Append("<td>");
                    Body.Append(" " + dr["PedidoEspecial"].ToString() + " ");
                    Body.Append("</td>");
              


                    Body.Append("<tr>");

            }

            Body.Append("</Table>");
           

            return Body.ToString();
        }


        private string GetSegundoReporte(int PeriodoId)
        {
            SqlParameter param = new SqlParameter();
            param.DbType = DbType.Int32;
            param.ParameterName = "@PeriodoID";
            param.Value = PeriodoId;
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(param);

            DataTable dt = ManagerDB.RunStoreProcedure(SPs.IndumentariaReporteAreaSinDetalle.ToString(), parametros);

            StringBuilder Body = new StringBuilder();




            Body.Append("<Table>");

            Body.Append("<tr>");
            Body.Append("<td>");
            Body.Append("<strong> AREA </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> DESCRIPCIÓN </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> TALLE </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> BRIGADISTA </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> CANTIDAD </strong> ");
            Body.Append("</td>");
            Body.Append("</tr>");

            foreach (DataRow dr in dt.Rows)
            {

                Body.Append("<tr style='background-color:#eeeeee'>");

                Body.Append("<td>");
                Body.Append(" " + dr["Area"].ToString() + "  ");
                Body.Append("</td>");

                Body.Append("<td>");
                Body.Append(" " + dr["Descripcion"].ToString() + " ");
                Body.Append("</td>");

                Body.Append("<td>");
                Body.Append(" " + dr["talle"].ToString() + " ");
                Body.Append("</td>");


                Body.Append("<td>");
                if (dr["Brigadista"].ToString().Trim().ToLower() == "true")
                    Body.Append("SI");
                else
                    Body.Append("NO");
                Body.Append("</td>");

                Body.Append("<td>");
                Body.Append(" " + dr["CANTIDAD"].ToString() + " ");
                Body.Append("</td>");

                Body.Append("<tr>");

            }

            Body.Append("</Table>");


            return Body.ToString();
        }


        private string GetTercerReporte(int PeriodoId)
        {
            SqlParameter param = new SqlParameter();
            param.DbType = DbType.Int32;
            param.ParameterName = "@PeriodoID";
            param.Value = PeriodoId;
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(param);

            DataTable dt = ManagerDB.RunStoreProcedure(SPs.IndumentariaReporteSinDetalle.ToString(), parametros);

            StringBuilder Body = new StringBuilder();




            Body.Append("<Table>");

            Body.Append("<tr>");
            Body.Append("<td>");
            Body.Append("<strong> DESCRIPCIÓN </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> TALLE </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> BRIGADISTA </strong> ");
            Body.Append("</td>");
            Body.Append("<td>");
            Body.Append("<strong> CANTIDAD </strong> ");
            Body.Append("</td>");
            Body.Append("</tr>");

            foreach (DataRow dr in dt.Rows)
            {

                Body.Append("<tr style='background-color:#eeeeee'>");

                Body.Append("<td>");
                Body.Append(" " + dr["Descripcion"].ToString() + " ");
                Body.Append("</td>");

                Body.Append("<td>");
                Body.Append(" " + dr["talle"].ToString() + " ");
                Body.Append("</td>");


                Body.Append("<td>");
                if (dr["Brigadista"].ToString().Trim().ToLower() == "true")
                    Body.Append("SI");
                else
                    Body.Append("NO");
                Body.Append("</td>");

                Body.Append("<td>");
                Body.Append(" " + dr["Cantidad"].ToString() + " ");
                Body.Append("</td>");

                Body.Append("<tr>");

            }

            Body.Append("</Table>");


            return Body.ToString();
        }



    }
}
