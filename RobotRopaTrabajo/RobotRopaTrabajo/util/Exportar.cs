﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RobotRopaTrabajo.util
{
    public class Exportar
    {
        StreamWriter w;

        string ruta;

        public string xpath

        {

            get { return ruta; }

            set { value = ruta; }

        }

 

        public Exportar(string path)

        {

            ruta = @path;

        }

        public void Export(StringBuilder cadenaFormateada)

        {

            try

            {

                FileStream fs = new FileStream(ruta, FileMode.Create, FileAccess.ReadWrite);

                w = new StreamWriter(fs, Encoding.UTF8);

                string comillas = char.ConvertFromUtf32(34);

                StringBuilder html = new StringBuilder();
                
                html.Append(cadenaFormateada);
                
                w.Write(html.ToString());

                w.Close();

            }

            catch (Exception ex)

            {

                throw ex;

            }

        }


        public string ExportaXLS(StringBuilder cadenaFormateada, string CarpetaDestino)
        {
            try
            {
                string ruta = CarpetaDestino + "\\Informe Ropa de Trabajo " + DateTime.Now.ToShortDateString().Replace("/","") + ".xls";
                Exportar OF = new Exportar(@ruta);

                OF.Export(cadenaFormateada);
                return ruta;   
            }
            catch (Exception ex)
            {                
                return "";
            }

            
        }
    }
}
