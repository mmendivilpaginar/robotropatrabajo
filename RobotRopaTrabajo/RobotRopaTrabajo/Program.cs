﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RobotRopaTrabajo.BussinesLayer;

namespace RobotRopaTrabajo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando Proceso...");
            ControllerProcess cp = new ControllerProcess();
            cp.Run();
            Console.WriteLine("Proceso Terminado.");
        }
    }
}
